/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/


#ifndef PRECED_H_
#define PRECED_H_

#include "stack.h"
#include "scaner.h"
#include "str.h"
#include "parser.h"
#include "ial.h"
#include "tlist.h"
#include <ctype.h>
#include <string.h>


extern tSymbolTable *table;
extern tListOfInstr *list;
extern int token;
extern string attr;
extern string act_func_name;
extern SetList function_list; 


typedef enum{  		//prvka na zasobniku
    SI_Do=DO,			            //		10
    SI_Then=THEN,		            //THEN		20
    SI_Comma=COMMA,		            // ,		40
    SI_LBrack=LEFT_BRACKET,	        // (		43
    SI_RBrack=RIGHT_BRACKET,	    // )		44
    SI_Semicolon=SEMICOLON,	        // ;		45
    SI_Add=PLUS,		            // +		46
    SI_Mult=MULTIPLE,		        // *		47
    SI_Div=SLASH,		            // /		48
    SI_Power=CARET,		            // ^		49
    SI_Great=GTR,		            // >		50
    SI_GrEq=GTR_EQL,		        // >=		51
    SI_Less=LESS,		            // <		52
    SI_LeEq=LESS_EQL,		        // <=		53
    SI_Assign=EQL,		            // =		54
    SI_Equal=EQUIV,		            // ==		55
    SI_NEqual=NON_EQUIV,	        // ~=		56
    SI_Concat=CONCAT,		        // ..		58
    SI_Sub=DASH,		            // -		59
    SI_Id=ID,			            // promena fce	80
    SI_Func=90,			            // pokud je ID fce
    SI_End=91,			            // $
    SI_SRule=92,			        // <(z���tek pravidla)
    SI_NonTerm=93
}SItems;

typedef enum{		//prvky v precedencni tabulce
    PT_Power,       // mocnina ^
    PT_MultDiv,     // nasobeni,deleni *,/
    PT_AddSub,      // scitani,odcitani +,-
    PT_Concat,      // konkatenace ..
    PT_Assign,      // prirazeni =
    PT_Compare,     // ekvivalence ==,=> <,>,<=,>=,~=,==
    PT_LeftBracket, // leva zavorka (
    PT_RightBracket,// prava zavorka )
    PT_Ident,       // identifikator, konstanta i
    PT_Func,        // type, find, sort, substr
    PT_Comma,       // carka ,
    PT_End          // konec $ ;,{
} PTParts;

typedef enum{
    PTErr=-1,           // chyba - error
    PTGtr,          // >
    PTLss,          // <
    PTEql          // =
} PTState;

int PTPart (int);
int PTRes (int, int);
int StTopTerm (TStack*);
tTableItem numStrToHT();
int genFunc(TList*);
int genRead();
int genSubstr();
int genSort();
int genFind();
int genType();
int ExpReduce(TStack*);
int exprSA(tTableItem*);
#endif
