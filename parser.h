/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

#ifndef PARSER_H_
#define PARSER_H_

#include <stdio.h>
#include "main.h"
#include "str.h"
#include "scaner.h"
#include "ial.h"
#include "ilist.h"

#include "prec_table.h"
#include "tlist.h"
// konstanty nutno overit jestli odpovidaji zadani
#define SYNTAX_OK     0

//#define SEM_OK        0
#define SYNTAX_ERROR  2
#define SEM_ERROR     3

#define UNIQUE     1
#define NOT_UNIQUE 0

// hlavicka pro syntakticky analyzator
int parse(tSymbolTable *ST, tListOfInstr *instrList);
void generateVariable(string *var,int unique);
void generateInstruction(int instType, void *addr1, void *addr2, void *addr3);
int func_def_and_dec();
int func();
int f_params();
int main_body();
int var_declaration();
int command_sequence();
int command();
int command_sequence_n();
int write();
int write_expr();
int write_expr_n();
int program();
void not_implemented();
void debug_exit();
#endif
