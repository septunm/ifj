DOKUMENTACE - README aneb jak z toho neud�lat n�jakou pras�rni�ku :) (xsoula02)

navrhuju p�id�vat do tohoto souboru texty, kter� budou jasn� ozna�en� od koho jsou a k �emu pat��
ka�d� tak nap�e svoje a pak to prochroust� jedna kompetentn� osoba, kter� tomu d� form�t, aby bylo v�echno stejn�

u� jsem ud�lal n�jakou kostru jak by to mohlo a m�lo vypadat, hodn� toho je naps�no v zad�n� a z toho jsem vych�zel
jako p��loha A budou metriky
p��loha B - kone�n� automa
p��loha c - LL gramatika, ta tam m� b�t taky podle zad�n�

+ vytvo�il wordovsk� styl "code", kter� m� Courier a budem do toho h�zet v�echny zdroj�ky, n�zvy funkc� apod.

rozsah m� b�t 4-7 stran, co� se po��t� bez p��loh (alespo�, tak m� to u�il Emil na st�edn�), tak�e v na�em p��pad� 7-10 str�nek, tak nem��em ps�t n�jak� rom�ny. 

v�ci, kter� by tam m�ly ur�it� b�t pops�ny je syntax, s�mantika, interpret a pak taky asi hash tabulka a mo�n� i lehounce sort a boyer-moore

v zad�n� je napsan�, �e dokumentace neslou�� k tomu, abychom tam napsali jak se �e�� nap�. alg. Booyer-Moore, ale k tomu jak jsme to �e�ili my a pop��pad�, kde byly probl�my atd. .. n�cot akov�ho jsem napsal do Lex. 

jak to ka�d�mu vyjde, tak do tohoto souboru p�id�vejte svoje texty zpracovan� cca podle t� dokumentace, kde jsem u� vytvo�il Lexik�ln� analyz�tor 

postupn� to budu do dokumentace p�id�vat a co p�id�m, tak odtud odma�u

N�pady v�t�ny, d�ky xsoula02




Boyer-Moore�v algoritmus

Boyer-Moore�v algoritmus slou�� k vyhled�n� libovoln�ho pod�et�zce v dan�m �et�zci. To m� nastarost vestav�n� funkce find. 
Algoritmus je �asov� velmi v�hodn�. Vyhled�v�n� �et�zec toti� porovn�v� od konce a proto je mo�n� p�i spln�n� podm�nek p�esko�it hned n�kolik znak�. Aby algoritmus mohl p�esko�it znaky hned o celou d�lku vyhled�v�n�ho �et�zce, mus� spl�ovat podm�nku, aby prvn� kontrolovan� znak nebyl nalezen v cel�m pod�et�zci, t�m doc�l�me, �e se posuneme v�dy za znak, kter� nesouhlas�. To z�rove� zn�men�, �e ��m je vyhled�van� �et�zec del��, t�m v�ce znak� m��eme p�esko�it a u�et��me t�m spoustu �asu s porovn�v�n�m znak�, kter� se nikdy nemohou rovnat vyhled�v�nemu �et�zci.
Pro vytvo�en� Boyer-Mooreova algoritmu n�m sou�ila zejm�na opora p�edm�tu IAL. Zvolili jsme druh� typ heurestiky, kter� zajist� je�t� v�t�� zv�hodn�n� algoritmu. D�le pro vytvo�en� algoritmu slou�ili dal�� matri�ly, kterou jsou obsa�eny mezi zdroji.  

http://en.wikipedia.org/wiki/Boyer%E2%80%93Moore_string_search_algorithm
http://home.avvanta.com/~doyle/bmi.html


QuickSort

Pro se�azen� v�ech prvk� v poli jsme podle zad�n� pou�ili algoritmus QuickSort. Tento algoritmus je pova�ov�n za velmi rychl�.
QuickSort funguje na principu zvolen� pivota. Pivot lze zvolit libovoln�, my jej ur�ili jako za��tek pole. Pot� je mo�n� p�eh�zet prvky tak, aby na jedn� stran� byly men�� prvky ne� pivot a na druh� stran� prvky v�t��, pivot je tedy p�esn� mezi t�mito ��sti. Tento princip jsme zvolili rekurzivn� pro ob� ��sti pole. Tato procedura se opakuje rekurzivn� tak dlouho, dokud neprojdeme v�echny prvky v poli. Pot� je pole se�azeno. 
QuickSort je z�visl� na vhodn�m zvolen� pivota. ��m men�� velikost pole, t�m je vhodn�j�� zvolit pivota krajn� prvek pole. Naopak p�i velk�m mno�stv� prvk� je vhodn� zvolit za pivota prost�edn� prvek.
 


