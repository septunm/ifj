CFLAGS=-std=c99 -lm -Wall -Wextra -pedantic -g
BIN=project_ifj
CC=gcc
RM=rm -f
FL=str.c ial.c ilist.c scaner.c parser.c interpret.c main.c prec_table.c stack.c cond_stack.c tlist.c
ALL: $(FL)
	$(CC) $(CFLAGS) $(FL) -o $(BIN)
	$(RM) *.o
