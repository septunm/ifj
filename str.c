/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "str.h"
#include "ilist.h"

#define STR_LEN_INC 8
// konstanta STR_LEN_INC udava, na kolik bytu provedeme pocatecni alokaci pameti
// pokud nacitame retezec znak po znaku, pamet se postupne bude alkokovat na
// nasobky tohoto cisla

#define STR_ERROR   1
#define STR_SUCCESS 0

int strInit(string *s)
// funkce vytvori novy retezec
{
   if ((s->str = (char*) malloc(STR_LEN_INC)) == NULL)
      return STR_ERROR;
   s->str[0] = '\0';
   s->length = 0;
   s->allocSize = STR_LEN_INC;
   return STR_SUCCESS;
}

void strFree(string *s)
// funkce uvolni retezec z pameti
{
    if(s->allocSize!=0)
        free(s->str);
}


int strClear(string *s)
// funkce vymaze obsah retezce
{
   s->str[0] = '\0';
   s->length = 0;
   if ((s->str = (char*) realloc(s->str, s->length + STR_LEN_INC)) == NULL)
         return STR_ERROR;
    s->allocSize=STR_LEN_INC;
    return STR_SUCCESS;
}

int strAddChar(string *s1, char c)
// prida na konec retezce jeden znak
{
   if (s1->length + 1 >= s1->allocSize)
   {
      // pamet nestaci, je potreba provest realokaci
      if ((s1->str = (char*) realloc(s1->str, s1->length + STR_LEN_INC)) == NULL)
         return STR_ERROR;
      s1->allocSize = s1->length + STR_LEN_INC;
   }
   s1->str[s1->length] = c;
   s1->length++;
   s1->str[s1->length] = '\0';
   return STR_SUCCESS;
}

// kontrola klicovych a rezervovanych slov, vraci jejich index +10 podle scanner.h
int checkKeywords(string *attr){
   char *keywords[23]={
    "do",
    "else",
    "end",
    "false",
    "function",
    "if",
    "local",
    "nil",
    "read",
    "return",
    "then",
    "true",
    "while",
    "write",
    "and",
    "break",
    "elseif",
    "for",
    "in",
    "not",
    "or",
    "repeat",
    "until"
    };

   for(int i=0;i<23;i++){
        if(strcmp(keywords[i],attr->str) == 0) return i+10;
   }
   return -1;

}

// hodnota stringu
char *valueString(string *str)
{
    if (str->str == NULL || str == NULL) {
        return "";
    }
    else return str->str;
}

// d�lka stringu
int lenghtString(string *str)
{
    if (str == NULL) {
        return -1;
    }
    else return str->length;
}

// kontrola zda je string prazdy nebo naopak
bool empty(string *string)
{
    if (string == NULL){
        return true;
    }
    if (string->length == 0 || string->length == -1){
        return true;
    }
    return false;
}

/* zkopirovani stringu do druheho
 * 1 - cilovy string - probehne alokace
 * 2 - string, ktery bude kopirovan
 */
bool copyString(string *cil_string, string *string)
{
    if(cil_string == NULL && string == NULL){
        return false;
    }
    if(string->allocSize == 0){
        // neni co kopirovat, pouze se nastavi cilovy string
        initString(cil_string);
        return true;
    }
    if(cil_string->allocSize == 0){
        // cilovy je prazdny, alokujeme
        cil_string->str = (char*) malloc(string->allocSize * sizeof(char));
    }
    else{
        // neni prazdny a proto musi probehnout realokace
        cil_string->str = (char*) realloc(cil_string->str, string->allocSize * sizeof(char));
    }
    if(cil_string->str == NULL){
        // alokovani probehlo neuspesne
        return false;
    }
    // zkopirovani stringu
    strcpy(cil_string->str, string->str);
    cil_string->allocSize = string->allocSize;
    cil_string->length    = string->length;
    return true;
}

// pocatecni nastaveni stringu
bool initString(string *str)
{
    if (str != NULL) {
        str->length = 0;
        str->allocSize = 0;
        str->str = NULL;
        return true;
    }
    return false;
}

/* Vytvori podretezec ze zadanych hodnot
 * 1 - prohledavany string
 * 2 - pocatecni hodnota podretezce (int od 1)
 * 3 - koncova hodnota podretezce
 */
string substr(string *str, int start, int konec)
{
    // delka puvodniho retezce
    int slen = str->length;
    // novy_string
    string sub_str;

    // nastaveni startu vzdy na kladne hodnoty
    if(start < 0) start = slen + start;
    else if(start > 0) start -= 1;
    else{
         // return - pr�zdn� string
         strInit(&sub_str);
         return sub_str;
    }
    // nastaveni konce vzdy na kaldne hodnoty
    if(konec < 0) konec = slen + konec +1;
    else if(konec == 0){
         // return - pr�zdn� string
         strInit(&sub_str);
         return sub_str;
    }

    // osetreni presahu stringu
    if(konec > slen) konec = slen;
    if(konec < 0) konec = 0;
    if(start < 0){
         // return - pr�zdn� string
         strInit(&sub_str);
         return sub_str;
    }
    if(start > slen){
         // return - pr�zdn� string
         strInit(&sub_str);
         return sub_str;
    }
    if(start > konec){
         // return - pr�zdn� string
         strInit(&sub_str);
         return sub_str;
    }

    // nastaveni velikosti podretezce + alokace
    int size = konec - start;
    char *buf = (char *)malloc((size+1) * sizeof(char));
    if(buf == NULL){
             //return OTHER_ERROR;
    }
    // zkopirovani podretezce do noveho retezce
    strncpy(buf, str->str+start, size);
    buf[size] = '\0';

    // ulozeni stringu + vraceni ukazatele
    sub_str.str = buf;
    sub_str.allocSize = (size+1) * sizeof(char);
    sub_str.length = size;
    return sub_str;
}

/* Lexikalni kontrola dvou retezcu
 * 1 - konstanta ze ilist.h
 * 2 - prvni string
 * 3 - druhy string
 */
bool LexCompare(int com, string *operand_1, string *operand_2){
    // kontrola rovnosti
    if(I_EQUIV == com || I_GTR_EQL == com || I_LESS_EQL == com){
               if(strcmp(operand_1->str, operand_2->str) == 0){
                    return true;
               }
    }
    // kontrola nerovnosti
    if(I_NON_EQUIV == com){
               if(strcmp(operand_1->str, operand_2->str) != 0){
                    return true;
               }
    }
    // >
    if(I_GTR == com || I_GTR_EQL == com){
              if(strcmp(operand_1->str, operand_2->str) > 0){
                    return true;
               }
    }
    // <
    if(I_LESS == com || I_LESS_EQL == com){
              if(strcmp(operand_1->str, operand_2->str) < 0){
                    return true;
               }
    }
    return false;
}
