/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/


#include "prec_table.h"

string PomAttr;
int genUn=0;
int stack=0;

static int PTable [12][12] = {
/*              ^[0]    *[1]    +[2]    ..[3]   =[4]    ==[5]   ( [6]   ) [7]   i [8]   f [9]   , [10]  $ [11]    */
/* ^ [0] */ {   PTLss,  PTGtr,  PTGtr,  PTGtr,  PTGtr,  PTGtr,  PTLss,  PTGtr,  PTLss,  PTLss,  PTGtr,  PTGtr   },
/* * [1] */ {   PTLss,  PTGtr,  PTGtr,  PTGtr,  PTGtr,  PTGtr,  PTLss,  PTGtr,  PTLss,  PTLss,  PTGtr,  PTGtr   },
/* + [2] */ {   PTLss,  PTLss,  PTGtr,  PTGtr,  PTGtr,  PTGtr,  PTLss,  PTGtr,  PTLss,  PTLss,  PTGtr,  PTGtr   },
/* ..[3] */ {   PTLss,  PTLss,  PTLss,  PTGtr,  PTGtr,  PTGtr,  PTLss,  PTGtr,  PTLss,  PTLss,  PTGtr,  PTGtr   },
/* = [4] */ {   PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTGtr,  PTLss,  PTLss,  PTGtr,  PTGtr   },
/* ==[5] */ {   PTLss,  PTLss,  PTLss,  PTLss,  PTGtr,  PTGtr,  PTLss,  PTGtr,  PTLss,  PTLss,  PTGtr,  PTGtr   },
/* ( [6] */ {   PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTEql,  PTLss,  PTLss,  PTEql,  PTErr   },
/* ) [7] */ {   PTGtr,  PTGtr,  PTGtr,  PTGtr,  PTGtr,  PTGtr,  PTErr,  PTGtr,  PTErr,  PTErr,  PTGtr,  PTGtr   },
/* i [8] */ {   PTGtr,  PTGtr,  PTGtr,  PTGtr,  PTGtr,  PTGtr,  PTErr,  PTGtr,  PTErr,  PTErr,  PTGtr,  PTGtr   },
/* f [9] */ {   PTErr,  PTErr,  PTErr,  PTErr,  PTErr,  PTErr,  PTEql,  PTErr,  PTErr,  PTErr,  PTErr,  PTErr   },
/* ,[10] */ {   PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTEql,  PTLss,  PTLss,  PTLss,  PTErr   },
/* $[11] */ {   PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTLss,  PTErr,  PTLss,  PTLss,  PTErr,  PTErr   }
};


int PTPart (int SI){
  switch(SI){
    case SI_Power: return PT_Power;
    case SI_Mult:
    case SI_Div: return PT_MultDiv;
    case SI_Add:
    case SI_Sub: return PT_AddSub;
    case SI_Concat: return PT_Concat;
    case SI_Assign: return PT_Assign;
    case SI_Equal:
    case SI_NEqual:
    case SI_Great:
    case SI_Less:
    case SI_GrEq:
    case SI_LeEq: return PT_Compare;
    case SI_LBrack: return PT_LeftBracket;
    case SI_RBrack: return PT_RightBracket;
    case SI_Id: return PT_Ident;
    case SI_Func: return PT_Func;
    case SI_End:
    case SI_Do:
    case SI_Then:
    case SI_Comma:
    case SI_Semicolon: return PT_End;
    default: return PTErr;
  }
}

int PTRes (int SIstack, int SItoken){  //vysledek z prec tabulky (<,>,=,Err)
  int PTstack=PTPart(SIstack);
  int PTtoken=PTPart(SItoken);
  if(PTstack==PTErr || PTtoken==PTErr){
    return PTErr;
  }
  return (PTable [PTstack][PTtoken]);
}

int StTopTerm (TStack *stack){  //vr�t� terminal nejbli� vrchu zasobniku
  TStackItem*Item=returnFirst_Item(stack);
  while(Item->token==SI_NonTerm){
    Item=Item->next;
  }
  return Item->token;
}

tTableItem numStrToHT(){
  string tmp;
  tTableItem PomHTItem;
  strInit(&tmp);
  copyString(&tmp, &attr);
  generateVariable(&attr, UNIQUE);
  if (genUn==0){
    strInit(&PomAttr);
    copyString(&PomAttr,&attr);
    genUn=1;
  }
  if (token==NUM){
    insertnewHT(table, &attr, Tnum);
    PomHTItem=findHT(table, attr.str);
    PomHTItem->value.Vnum=strtod(tmp.str, NULL);
  }
  else{
    insertnewHT(table, &attr, Tstr);
    PomHTItem=findHT(table, attr.str);
    strInit(&(PomHTItem->value.Vstr));
    copyString(&(PomHTItem->value.Vstr),&tmp);//vlozeni string attr do HTItem->value.Vstr
  }
  strFree(&tmp);
  return PomHTItem;
}

int genFunc(TList* func){
  tTableItem HTItem,PomHTItem;
  strInit(&PomAttr);
  generateVariable(&PomAttr, UNIQUE);
  genUn=1;
  insertnewHT(table, &PomAttr, Tundef);
  HTItem=findHT(table, PomAttr.str);


  if((token=getNextToken(&attr))!=SI_LBrack){ // pokud neni func(  tak SYNTAX_ERR
if(DEBUG)printf("chyba v parametrech fce");
    return SYNTAX_ERROR;
  }

  token=getNextToken(&attr);
  //vic nez 0 param
  int i=0;
  ParamItem* ParamI=func->first;

  while (func->count_params > i){
    if(token!=SI_RBrack){  //pokud uz sou vy�erpany paramatry, nastavi se ostantni na nil
      if(token==SI_Comma){
        token=getNextToken(&attr);
	if(token==SI_RBrack){  //pokud je func(E,)  tak chyba
	  return SYNTAX_ERROR;
	}
      }
      if(token==NUM || token==STR){
        PomHTItem=numStrToHT();
      }
      else if(token==ID){
        generateVariable(&attr,NOT_UNIQUE);
        if ((PomHTItem=findHT(table, attr.str))==NULL){
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genFunc ->spatne ID u func() ");
          return SEM_ERROR;
        }
      }
      else{  // pokud neni E str, num nebo ID
        return SYNTAX_ERROR;
      }
      generateInstruction(I_SETVAR,ParamI->adr,PomHTItem,NULL);

      token=getNextToken(&attr);


    }
    i++;
    ParamI=ParamI->next;
  }

  //vic param nez ne ve funkci deklarovane(zanedbani)
  while(token==SI_Comma){
   token=getNextToken(&attr);
   if(token==NUM || token==STR || token==ID){
     token=getNextToken(&attr);
   }
   else{  // pokud neni E str, num nebo ID
     return SYNTAX_ERROR;
   }

  }
  //konec vic param

  if(token!=SI_RBrack){  //pokud neni func(E,..,E) tak SYNTAX_ERR
if(DEBUG)printf("chyba v parametrech fce    4 +++++++");
    return SYNTAX_ERROR;
  }

  generateInstruction(I_FUNC_JMP,func->instr_adr,NULL,NULL);
  generateInstruction(I_SETVAR,HTItem,func->table,NULL);
  //VYGENEROVANI SKOKU NA FUNKCI
  token=SI_Id;
  copyString(&attr,&PomAttr);
  strFree(&PomAttr);
  return SYNTAX_OK;

}

// I_READ_NUM  I_READ_STR_EOF  I_READ_STR_EOL  I_READ_STR_CNT
int genRead(){
  tTableItem HTItem;
  strInit(&PomAttr);
  generateVariable(&PomAttr, UNIQUE);
  genUn=1;
  insertnewHT(table, &PomAttr, Tundef);
  HTItem=findHT(table, PomAttr.str);

  if((token=getNextToken(&attr))!=SI_LBrack){ // pokud neni read(  tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }

  token=getNextToken(&attr);
  if (token==NUM){  //nacte dany pocet (kladne cislo) znaku -- vraci Tstr
    tTableItem HTItem2;
    HTItem->type=Tstr;
    strInit(&HTItem->value.Vstr);
    HTItem2=numStrToHT();
    generateInstruction(I_READ_STR_CNT,HTItem,HTItem2,NULL);
  }
  else if (token==STR){
    if(strcmp(attr.str,"*l")==0){  //nacte vsechno az po EOL -- vraci Tstr
      HTItem->type=Tstr;
      strInit(&HTItem->value.Vstr);
      generateInstruction(I_READ_STR_EOL,HTItem,NULL,NULL);
    }
    if(strcmp(attr.str,"*a")==0){  //nacte vsechno az po EOF -- vraci Tstr
      HTItem->type=Tstr;
      strInit(&HTItem->value.Vstr);
      generateInstruction(I_READ_STR_EOF,HTItem,NULL,NULL);
    }
    if(strcmp(attr.str,"*n")==0){  //nacte cislo -- vraci Tnum
      HTItem->type=Tnum;
      generateInstruction(I_READ_NUM,HTItem,NULL,NULL);
    }
  }
  else{  // E neni kladne cislo, "*n", "*l" ani "*a" takze SYNTAX_ERR
    return SYNTAX_ERROR;
  }

  token=getNextToken(&attr);
  if(token!=SI_RBrack){  //pokud neni read(E) tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }
  token=SI_Id;
  copyString(&attr,&PomAttr);
  strFree(&PomAttr);
  return SYNTAX_OK;
}


 //substr:
/*****  (I_SUBSTR, vysledek, NULL, NULL)   *****/
/*****  (I_SUBSTR, retezec, zacatek podretezce, konec podretezce)   *****/
int genSubstr(){
  tTableItem HTItem,HTItemStr,HTItemSt,HTItemEnd;
  strInit(&PomAttr);
  generateVariable(&PomAttr, UNIQUE);
  genUn=1;
  insertnewHT(table, &PomAttr, Tundef);
  HTItem=findHT(table, PomAttr.str);
  if((token=getNextToken(&attr))!=SI_LBrack){ // pokud neni substr(  tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }
//1.param
  token=getNextToken(&attr);
  if(token==STR){
    HTItemStr=numStrToHT();
  }
  else if(token==ID){
    generateVariable(&attr,NOT_UNIQUE);
    if ((HTItemStr=findHT(table, attr.str))==NULL){
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genSubstr ->spatny prvni pramaetr u substr()(ID neni deklarovano) ");
      return SEM_ERROR;
    }
  }
  else{  // pokud neni E str nebo ID
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genSubstr ->spatny prvni pramaetr u substr()");
    return SYNTAX_ERROR;
  }


  token=getNextToken(&attr);
  if(token!=SI_Comma){  //pokud neni find(E, tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }

//2.param
  token=getNextToken(&attr);
  if(token==NUM){
    HTItemSt=numStrToHT();
  }
  else if(token==ID){
    generateVariable(&attr,NOT_UNIQUE);
    if ((HTItemSt=findHT(table, attr.str))==NULL){
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genSubstr ->spatny 2. param u substr() ");
      return SEM_ERROR;
    }
  }
  else{  // pokud neni E num nebo ID
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genSubstr ->spatny 2. param u substr() ");
    return SYNTAX_ERROR;
  }

  token=getNextToken(&attr);
  if(token!=SI_Comma){  //pokud neni find(E,E, tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }

//3.param
  token=getNextToken(&attr);
  if(token==NUM){
    HTItemEnd=numStrToHT();
  }
  else if(token==ID){
    generateVariable(&attr,NOT_UNIQUE);
    if ((HTItemEnd=findHT(table, attr.str))==NULL){
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genSubstr ->spatny 3. param u substr() ");
      return SEM_ERROR;
    }
  }
  else{  // pokud neni E num nebo ID
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genSubstr ->spatny 3. param u substr() ");
    return SYNTAX_ERROR;
  }
  token=getNextToken(&attr);
  if(token!=SI_RBrack){  //pokud neni find(E,E,E) tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }
  generateInstruction(I_SUBSTR,HTItem,NULL,NULL);
  generateInstruction(I_SUBSTR,HTItemStr,HTItemSt,HTItemEnd);
  token=SI_Id;
  copyString(&attr,&PomAttr);
  strFree(&PomAttr);
  return SYNTAX_OK;
}

//sort:
/*****  (I_SORT, vysledek, retezec, NULL)   *****/
int genSort(){
  tTableItem HTItem,HTItem2;
  strInit(&PomAttr);
  generateVariable(&PomAttr, UNIQUE);
  genUn=1;
  insertnewHT(table, &PomAttr, Tundef);
  HTItem=findHT(table, PomAttr.str);
  if((token=getNextToken(&attr))!=SI_LBrack){ // pokud neni type(  tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }
  token=getNextToken(&attr);
  if(token==STR){
    HTItem2=numStrToHT();
  }
  else if(token==ID){
    generateVariable(&attr,NOT_UNIQUE);
    if ((HTItem2=findHT(table, attr.str))==NULL){
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genType ->spatne ID u sort() ");
      return SEM_ERROR;
    }
  }
  else{  // pokud neni E str nebo ID
    return SYNTAX_ERROR;
  }
  token=getNextToken(&attr);
  if(token!=SI_RBrack){  //pokud neni type(E) tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }
  generateInstruction(I_SORT,HTItem,HTItem2,NULL);
 // addItem_Stack(stack, SI_Id, HTItem);
  token=SI_Id;
  copyString(&attr,&PomAttr);
  strFree(&PomAttr);
  return SYNTAX_OK;
}


//find:
/*****  (I_FIND, vysledek, retezec, hledany podretezec)   *****/
int genFind(){
  tTableItem HTItem,HTItemStr,HTItemFind;
  strInit(&PomAttr);
  generateVariable(&PomAttr, UNIQUE);
  genUn=1;
  insertnewHT(table, &PomAttr, Tundef);
  HTItem=findHT(table, PomAttr.str);
  if((token=getNextToken(&attr))!=SI_LBrack){ // pokud neni find(  tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }
//1.param
  token=getNextToken(&attr);
  if(token==STR){
    HTItemStr=numStrToHT();
  }
  else if(token==ID){
    generateVariable(&attr,NOT_UNIQUE);
    if ((HTItemStr=findHT(table, attr.str))==NULL){
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genFind ->spatny prvni pramaetr u find()(ID neni deklarovano) ");
      return SEM_ERROR;
    }
  }
  else{  // pokud neni E str nebo ID
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genFind ->spatny prvni pramaetr u find()");
    return SYNTAX_ERROR;
  }


  token=getNextToken(&attr);
  if(token!=SI_Comma){  //pokud neni find(E, tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }

//2.param
  token=getNextToken(&attr);
  if(token==STR){
    HTItemFind=numStrToHT();
  }
  else if(token==ID){
    generateVariable(&attr,NOT_UNIQUE);
    if ((HTItemFind=findHT(table, attr.str))==NULL){
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genType ->spatny 2. param u find() ");
      return SEM_ERROR;
    }
  }
  else{  // pokud neni E str nebo ID
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genType ->spatny 2. param u find() ");
    return SYNTAX_ERROR;
  }
  token=getNextToken(&attr);
  if(token!=SI_RBrack){  //pokud neni find(E,E) tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }
  generateInstruction(I_FIND,HTItem,HTItemStr,HTItemFind);
  token=SI_Id;
  copyString(&attr,&PomAttr);
  strFree(&PomAttr);
  return SYNTAX_OK;
}

//type:
/*  instukce (I_TYPE, adr1, adr2, NULL)
            adr1 - ulozeni typu promenne adr2
            adr2 - promenna, jejiz typ zjistujeme
        */
int genType(){
  tTableItem HTItem,HTItem2;
  strInit(&PomAttr);
  generateVariable(&PomAttr, UNIQUE);
  genUn=1;
  insertnewHT(table, &PomAttr, Tundef);
  HTItem=findHT(table, PomAttr.str);
  if((token=getNextToken(&attr))!=SI_LBrack){ // pokud neni type(  tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }
  token=getNextToken(&attr);
  if(token==NUM || token==STR){
    HTItem2=numStrToHT();
  }
  else if(token==ID){
    generateVariable(&attr,NOT_UNIQUE);
    if ((HTItem2=findHT(table, attr.str))==NULL){
if(DEBUG)printf("+++++++++++++++ERROR v expSU-> genType ->spatne ID u type() ");
      return SEM_ERROR;
    }
  }
  else{  // pokud neni E str, num nebo ID
    return SYNTAX_ERROR;
  }
  token=getNextToken(&attr);
  if(token!=SI_RBrack){  //pokud neni type(E) tak SYNTAX_ERR
    return SYNTAX_ERROR;
  }
  generateInstruction(I_TYPE,HTItem,HTItem2,NULL);
  token=SI_Id;
  copyString(&attr,&PomAttr);
  strFree(&PomAttr);
  return SYNTAX_OK;
}

//###############################################################################
//##    pravidla pro redukci NonTerminalu+Semant kontroly+generovani instr.    ##
int ExpReduce(TStack* stack){
    int SITerm;
    if((SITerm=StTopTerm(stack))!=SI_SRule){
        TStackItem*Item=returnFirst_Item(stack);
        TStackItem*Item2;
        string tmp;
        strInit(&tmp);
        tTableItem HTItem;
        switch(SITerm){
	case SI_Id:
		Item->token=SI_NonTerm;
		Item2=Item->next;
		Item->next=Item->next->next;
		freeItem_Stack(stack, Item2);
		break;
	case SI_Add:
		if(Item->next->next->token!=SI_NonTerm){//kontrola jestli je pred + E
		  return SYNTAX_ERROR;
		}
                Item2=Item->next->next;
                generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tnum);
                HTItem=findHT(table, tmp.str);
                HTItem->value.Vnum=0;
                generateInstruction(I_ADD,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E+E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_Sub:
                if(Item->next->next->token!=SI_NonTerm){//kontrola jestli je pred - E
		  return SYNTAX_ERROR;
		}
		Item2=Item->next->next;
                generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tnum);
                HTItem=findHT(table, tmp.str);
                HTItem->value.Vnum=0;
                generateInstruction(I_SUB,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E-E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_Mult:
                if(Item->next->next->token!=SI_NonTerm){//kontrola jestli je pred * E
		  return SYNTAX_ERROR;
		}
		Item2=Item->next->next;
                generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tnum);
                HTItem=findHT(table, tmp.str);
                HTItem->value.Vnum=0;
                generateInstruction(I_MUL,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E*E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_Div:
                if(Item->next->next->token!=SI_NonTerm){//kontrola jestli je pred / E
		  return SYNTAX_ERROR;
		}
		Item2=Item->next->next;

                generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tnum);
                HTItem=findHT(table, tmp.str);
                HTItem->value.Vnum=0;
                generateInstruction(I_DIV,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E/E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_RBrack:
		Item2=Item->next;
		if(Item2->token!=SI_NonTerm){//kontrola jestli je pred ) E
		  return SYNTAX_ERROR;
		}
		HTItem=Item2->table;
                freeXItem(stack,4);              //odstrani ze zasobniku <(E)
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_Power:
                Item2=Item->next->next;
		if(Item2->token!=SI_NonTerm){//kontrola jestli je pred ) E
		  return SYNTAX_ERROR;
		}
		generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tnum);
                HTItem=findHT(table, tmp.str);
		HTItem->value.Vnum=0;
                generateInstruction(I_POWER,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E^E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_Assign:
                Item2=Item->next->next;
		if(Item2->token!=SI_NonTerm){//kontrola jestli je pred = E
		  return SYNTAX_ERROR;
		}
		//semant. kontrola jestli se prirazuje do promenne
		int i=0;
                while(Item2->table->name.str[i]!='$'){
		  i++;
		}
		if(Item2->table->name.str[i+1]=='$' || isdigit(Item2->table->name.str[i+1])){
		  return SEM_ERROR;
                } //konec sem. kontroly

		HTItem=Item2->table;
                generateInstruction(I_SETVAR,Item2->table,Item->table,NULL);
                freeXItem(stack,4);              //odstrani ze zasobniku <E=E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_Concat:
                Item2=Item->next->next;
		if(Item2->token!=SI_NonTerm){//kontrola jestli je pred .. E
		  return SYNTAX_ERROR;
		}
		generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tundef);
                HTItem=findHT(table, tmp.str);
	//	strInit(&(HTItem->value.Vstr));
                generateInstruction(I_CONCAT,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E..E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_Equal:
                Item2=Item->next->next;
		if(Item2->token!=SI_NonTerm){//kontrola jestli je pred == E
		  return SYNTAX_ERROR;
		}
		generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tbool);
                HTItem=findHT(table, tmp.str);
		HTItem->value.Vbool=0;
                generateInstruction(I_EQUIV,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E==E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_NEqual:
                Item2=Item->next->next;
		if(Item2->token!=SI_NonTerm){//kontrola jestli je pred ~= E
		  return SYNTAX_ERROR;
		}
		generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tbool);
                HTItem=findHT(table, tmp.str);
		HTItem->value.Vbool=0;
                generateInstruction(I_NON_EQUIV,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E~=E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_Great:
                Item2=Item->next->next;
		if(Item2->token!=SI_NonTerm){//kontrola jestli je pred > E
		  return SYNTAX_ERROR;
		}
		generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tbool);
                HTItem=findHT(table, tmp.str);
		HTItem->value.Vbool=0;
                generateInstruction(I_GTR,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E>E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_GrEq:
                Item2=Item->next->next;
		if(Item2->token!=SI_NonTerm){//kontrola jestli je pred >= E
		  return SYNTAX_ERROR;
		}
		generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tbool);
                HTItem=findHT(table, tmp.str);
		HTItem->value.Vbool=0;
                generateInstruction(I_GTR_EQL,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E>=E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_Less:
                Item2=Item->next->next;
		if(Item2->token!=SI_NonTerm){//kontrola jestli je pred < E
		  return SYNTAX_ERROR;
		}
		generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tbool);
                HTItem=findHT(table, tmp.str);
		HTItem->value.Vbool=0;
                generateInstruction(I_LESS,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E<E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	case SI_LeEq:
                Item2=Item->next->next;
		if(Item2->token!=SI_NonTerm){//kontrola jestli je pred <= E
		  return SYNTAX_ERROR;
		}
		generateVariable(&tmp, UNIQUE);
                insertnewHT(table, &tmp, Tbool);
                HTItem=findHT(table, tmp.str);
		HTItem->value.Vbool=0;
                generateInstruction(I_LESS_EQL,HTItem,Item2->table,Item->table);
                freeXItem(stack,4);              //odstrani ze zasobniku <E<=E
                addItem_Stack(stack, SI_NonTerm, HTItem);
                break;
	}
	strFree(&tmp);
    }
    else{
        if(DEBUG)printf("++++++++ERROR v EXpREDUCE++++++++++++++++");
        return SEM_ERROR;
    }
    return SYNTAX_OK;
}



//############################################
//###  Hlavn� t�lo PRECEDEN�N� ANAL.  ########
//############################################
int exprSA(tTableItem* HTItem){
  //vlozeni $na zasobnik
  //porovnani vrchniho terminalu s tokenem pomoci PTRes
  //vlozeni na zabobnik nebo pouziti pravidla, nacitani tokenu..
  TStack Stack;
  initStack(&Stack);
  tTableItem PomHTItem;
if(DEBUG)printf("##################ZACATEK exprSA ####################");
  addItem_Stack(&Stack,SI_End,NULL); //vlo�en� 1. $ na vrchol zasobn�ku(ukazuje dno)

  while(1){
if(DEBUG)printf("\n\n+++++++akt.token:%d  ++++++++top term:%d\n", token,StTopTerm(&Stack));

    if ((StTopTerm(&Stack)==SI_End) && (token)==SI_RBrack){ //(token==RIGHT_BRACKET)){
      TStackItem*Item=returnFirst_Item(&Stack);
      *HTItem=Item->table;
if(DEBUG)printf("RETURNUJU ITEM typ:%d\n", (*HTItem)->type);
if(DEBUG)printf("################## RETURN z exprSA  ERROR, ale token je \")\"  #########");
    freeStack(&Stack);
      return SYNTAX_ERROR;          //1;   ----------------------------
    }

    if ((StTopTerm(&Stack)==SI_End) && (PTPart(token)==PT_End)){ //(token==SEMICOLON  || token==DO || token ==THEN)){
      TStackItem*Item=returnFirst_Item(&Stack);
      *HTItem=Item->table;
if(DEBUG)printf("RETURNUJU ITEM typ:%d\n token:%d", (*HTItem)->type,token);
if(DEBUG)printf("################## RETURN z exprSA  OK  #########");
    freeStack(&Stack);
      return SYNTAX_OK;          //1;   ----------------------------
    }

    if (token==SI_Id && (strcmp(attr.str,"type")==0 || strcmp(attr.str,"substr")==0 || strcmp(attr.str,"find")==0 || strcmp(attr.str,"sort")==0)){
      if(strcmp(attr.str,"type")==0){
        int ireturn;
	if((ireturn=genType())!=SYNTAX_OK){
	  return ireturn;
	}
      }
      if(strcmp(attr.str,"substr")==0){
        int ireturn;
	if((ireturn=genSubstr())!=SYNTAX_OK){
	  return ireturn;
	}

      }
      if(strcmp(attr.str,"find")==0){
        int ireturn;
	if((ireturn=genFind())!=SYNTAX_OK){
	  return ireturn;
	}

      }
      if(strcmp(attr.str,"sort")==0){
        int ireturn;
	if((ireturn=genSort())!=SYNTAX_OK){
	  return ireturn;
	}
      }
    }

    else if (token==SI_Id){
      TList *func;
      if((func=findTList(&function_list,&attr))!=NULL){
        int ireturn;
        if((ireturn=genFunc(func))!=SYNTAX_OK){
          return ireturn;
	}
      }
    }

    else if (token==NUM || token==STR){
      PomHTItem=numStrToHT();
      token=SI_Id;
      copyString(&attr,&PomAttr);
      strFree(&PomAttr);
    }

    else if (token==READ){
if(DEBUG)printf("V read\n");
      int ireturn;
      if((ireturn=genRead())!=SYNTAX_OK){
        return ireturn;
      }
    }


if(DEBUG)printf("pred switch\n");
    switch(PTRes(StTopTerm(&Stack),token)){
       //V�SLEDEK PREC. TABULKY='<'
        case (PTLss):{   //  push na zasobnik SI_SRule,push na zasobnik Aktualni token
		TStackItem*Item=returnFirst_Item(&Stack);
        if(DEBUG)printf("vrchny Term/ NonTerm%d\n",Item->token);
     		if(Item->token!=SI_NonTerm){                 //pokud na vrchu zasobniku neni NonTerm
    		  addStSRule_Stack(&Stack, 0);              //push < na vrchol zasobniku
if(DEBUG)printf("\n++++++++++++Vlozeni na vrch+++++++++++++SI_SRule\n");
      		}
      		else{
        	  addStSRule_Stack(&Stack, 1);              //push < na 2 misto od vrchu zasobniku (na 1. je NonTerm)
if(DEBUG)printf("\n++++++++++++Vlozeni na 2.misto+++++++++++++SI_SRule\n");
      		}

      		if(token==SI_Id || token==SI_Func){
		  if(genUn==0){
		    generateVariable(&attr,NOT_UNIQUE);
		  }
        	  if ((PomHTItem=findHT(table, attr.str))==NULL){

if(DEBUG)printf("+++++++++++++++ERROR v expSU prec table <");

          	    return SEM_ERROR;
        	  }
       		  addItem_Stack(&Stack,token,PomHTItem);
      		}
      		else{
        	  addItem_Stack(&Stack,token,NULL);
      		}
                genUn=0;
      		token=getNextToken(&attr);
if(DEBUG)printf("++++++++++++++++++++++++++++\n+++++++++++Vysl prec.table '<'+++++\n+++++++++++++++++++\n");
if(DEBUG)viewStack(&Stack);
if(DEBUG)printf("\n+++++++++++++++++++++++++++++\n<<<<<<<<<<<<<<<<<<<<<<\n");
     		break;
    	}


        //V�SLEDEK PREC. TABULKY='>'
    	case PTGtr:{   //  ExpReduce
      		int Experr=ExpReduce(&Stack);
		if(Experr!=SYNTAX_OK){
		  return Experr;
		}
if(DEBUG)printf("++++++++++++++++++++++++++++\n+++++++++++Vysl prec.table '>'+++++\n+++++++++++++++++++\n");
if(DEBUG)viewStack(&Stack);
if(DEBUG)printf("\n+++++++++++++++++++++++++++++\n>>>>>>>>>>>>>>>>>>>>>>>>>\n");
     		break;
   	}


       //V�SLEDEK PREC. TABULKY='='
    	case PTEql:{    //  push na zabobnik Aktualni token
      		if(token==SI_Id || token==SI_Func){
		  if(genUn==0){
		    generateVariable(&attr,NOT_UNIQUE);
		  }
		  if ((PomHTItem=findHT(table, attr.str))==NULL){
          	    return SEM_ERROR;
        	  }
        	  addItem_Stack(&Stack,token,PomHTItem);
      		}
      		else{
        	  addItem_Stack(&Stack,token,NULL);
      		}
		genUn=0;
      		token=getNextToken(&attr);
if(DEBUG)printf("++++++++++++++++++++++++++++\n+++++++++++Vysl prec.table '='+++++\n+++++++++++++++++++\n");
if(DEBUG)viewStack(&Stack);
if(DEBUG)printf("\n+++++++++++++++++++++++++++++\n========================\n");
    		break;
   	}


       //V�SLEDEK PREC. TABULKY='prazdne misto'
    	case PTErr:{     //  return syntax error
if(DEBUG)printf("+++++++++++++++ERROR v expSU prazdny misto tabulky");
      		return SEM_ERROR;
    	}

    }
  }
}
