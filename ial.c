/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

#include "ial.h"
#define MAX_ASCII_SIZE 255 // maximalni velikost ascii

/* Nalezeni podretece, fce vraci index pocatecniho pismene (poc. od 1)
 * 1 - string
 * 2 - vyhledavany podretezec
 */
int find(string *str, string *search){
    // pokud je hledany prazdny - ukonceni fce
    if(lenghtString(search) == 0){
        return 0;
    }

    // nastaveni chyboveho stavu
    int no_exist = -1;

    // pomocne pole
    int ascii_size[MAX_ASCII_SIZE];
    int search_size[search->length+1];

    // pomocne info k hledanemu = hodnota + delka
    char *searchStr = valueString(search);
    int searchLength = search->length;

    // pomocne info k retezci = hodnota + delka
    char *strStr = valueString(str);
    int strLength = str->length;

    // inicializace obou poli
    ascii_p(ascii_size, searchStr, searchLength);
    search_p(search_size, searchStr, searchLength);


    // kontrola velikosti obou poli - zda je mozny regulerni vysledek
    if((strLength <= 0) || (searchLength <= 0) || (strLength < searchLength)){
       return no_exist;
    }
    else{
       // pocatecni pozice
       int strPozice    = 0;
       int searchPozice = searchLength-1;

       while(strPozice <= strLength - searchLength){
            // while - dokud je legalni pozice
            while(searchStr[searchPozice] == strStr[strPozice+searchPozice]){
                // dokud jsou stejne znaky
                if(searchPozice == 0){
                  // jsme na konci retezce - return pozice - +1 (je pocitano od jednicky)
                  return strPozice+1;
                }
                // jinak se snizi pozice
                searchPozice--;
            }
            //posuneme pattern vuci textu dale podle vysledku - vybereme vetsi mozny posun
            if(ascii_size[(int)strStr[strPozice+searchLength-1]]>search_size[searchPozice+1]){
                 strPozice += ascii_size[(int)strStr[strPozice+searchLength-1]];
            }
            else strPozice += search_size[searchPozice+1];

            searchPozice = searchLength-1;  // posun na konec stringu
       }
    }

    return no_exist;
}

// inicializace pole = CharJamp(IAL)
void ascii_p(int ascii_pole[], char *str, int length){
    int i;
    for(i=0; i<MAX_ASCII_SIZE; i++){
       ascii_pole[i] = length;
    }
    for(i=0; i<length-1; i++){
       ascii_pole[(int)str[i]] = length-i-1;
    }
}

// druha heureistika - nastaveni retezce proti sobe = MatchJump(IAL)
void search_p(int search_pole[], char *search_str, int search_length){
    // pomocne pole delky
    int pole_length[search_length+1];
    int pozice_1;
    int pozice_2;

    // inicializace pole
    for(pozice_1=0; pozice_1<search_length+1; pozice_1++){
        search_pole[pozice_1] = 0;
        pole_length[pozice_1] = 0;
    }

    // nastaveni pocatecnich pozic
    pozice_1 = search_length;
    pozice_2 = search_length + 1;
    pole_length[pozice_1] = pozice_2;

    //hledame opakovane podretezce v patternu
    while(pozice_1>0){
        // dokud nejsme na zacatku retezce - prochazeni od zadu
        while((search_str[pozice_2-1]!=search_str[pozice_1-1]) && (pozice_2<=search_length) ){

            if(search_pole[pozice_2] == 0){
               search_pole[pozice_2] = pozice_2-pozice_1;
            }
            pozice_2 = pole_length[pozice_2];

        }
        -- pozice_1;
        -- pozice_2;
        // nastaveni nove pozice
        pole_length[pozice_1] = pozice_2;
    }

    pozice_2 = pole_length[0];
    for(pozice_1=0; pozice_1<=search_length; pozice_1++){
        if(search_pole[pozice_1] == 0){
           search_pole[pozice_1] = pozice_2;
        }
        if(pozice_1 == pozice_2){
           pozice_2 = pole_length[pozice_2];
        }
    }
}


// serazeni retezce string - fce vraci ukazatel na zkopirovanou ztrukturu (novou)
string sort(string *sort){
    string novy_str;
    strInit(&novy_str);
    if(empty(sort)){
        return novy_str; // return; - puvodni retezec
    }

    if(!copyString(&novy_str, sort)){
        return novy_str; // return; - puvodni retezec
    }

    quickSort(novy_str.str, 0, strlen(novy_str.str)-1);

    return novy_str;
}

/* Serazeni retezce - quick sort
 * 1 - pole
 * 2 - prvni index
 * 3 - (pocet znaku - 1)
 */

void quickSort(char *pole, int start, int konec){
     int i = start;
     int k = konec;
     if (konec - start >= 1){
        int pivot = *(pole+start);

        while (k > i){
              while ( (*(pole+i) <= pivot) && (i <= konec) && (k > i) )
                 i++;
              while ( (*(pole+k) > pivot) && (k >= start) && (k >= i) )
                 k--;
              if (k > i){
                 int pomoc = *(pole+i);
                 *(pole+i) = *(pole+k);
                 *(pole+k) = pomoc;
              }
        }
        int pomoc = *(pole+start);
        *(pole+start) = *(pole+k);
        *(pole+k) = pomoc;
        quickSort(pole, start, k - 1);
        quickSort(pole, k + 1, konec);
     }
}

//inicializuje prvky pole Tabulky symbolu na NULL
void initHT(tSymbolTable* table)
{
  for (int i=0; i<TABLEMAX; i++){
    table->items[i]=NULL;
  }
}

//ziskani indexu ze jm�na
int hash(char* name)
{
  int size= strlen(name);
  int hash=0;
  for(int i=0; i<size; i++){
    hash+=name[i];
  }
  hash=hash%TABLEMAX;
  return hash;
}


int insertnewHT(tSymbolTable* table,  string *name, Ttype type){

    tTableItem found = findHT(table,name->str);

    if(found == NULL){
        tTableItem temp;
        string new_name;
        //exit(EXIT_FAILURE);
        temp = malloc(sizeof(struct TableItem) + name->length*sizeof(char *)); // alokace prostoru

        int index = hash(name->str);
        strInit(&new_name);
        copyString(&new_name,name);
        temp->name = new_name;// naplneni daty
        temp->type = type;
        temp->declar = true;
        temp->used = false;
        temp->next = table->items[index];
        table->items[index] = temp;
        if(DEBUG){printf("--INSERTNEWHT--name: %s hash:%d \n", temp->name.str, index);}
        return SEM_OK;
    }
    else return SEM_ERROR;
}

tTableItem findHT (tSymbolTable* table, char* name)
{
  int index=hash(name);
  tTableItem PomItem=table->items[index];
  while (PomItem!=NULL){
    if (strcmp(name,PomItem->name.str)==0){
      return PomItem;
    }
    PomItem=PomItem->next;
  }
  return NULL;
}

int removeItemHT (tSymbolTable* table, char* name)
{
  int index=hash(name);
  tTableItem PomItem=table->items[index];
  tTableItem PomItem2=table->items[index];
  while (PomItem!=NULL){
    if (strcmp(name,PomItem->name.str)==0){
      PomItem2->next=PomItem->next;
      if(PomItem->type==Tstr)
        strFree(&(PomItem->value.Vstr));
      free(PomItem);
      return EXIT_SUCCESS;
    }
    PomItem2=PomItem;
    PomItem=PomItem->next;
  }
  return EXIT_FAILURE;
}

void destroyHT (tSymbolTable* table)
{
int dest=0;
  tTableItem PomItem;
  for (int i=0; i<TABLEMAX; i++){
    PomItem=table->items[i];
    while (PomItem!=NULL){
      table->items[i]=PomItem->next;
      strFree(&PomItem->name);
      if(PomItem->type==Tstr)
        strFree(&(PomItem->value.Vstr));
      free(PomItem);
      PomItem=table->items[i];
//debug
if(DEBUG){dest++;printf("--DESTROY--zniceno:%d \n",dest);}
    }
  }
}
void setStringValueHT(tTableItem table_item,string *val){
    tTableItem tmp;
    tmp=realloc(table_item, sizeof(struct TableItem) + val->length*sizeof(char));
    copyString(&tmp->value.Vstr,val);
    tmp->type=table_item->type;
    tmp->name=table_item->name;
    tmp->declar=table_item->declar;
    tmp->used=table_item->used;
    tmp->next=table_item->next;
    table_item=tmp;
}


// vytiskni celou tabulku

void PrintHT( tSymbolTable* table ) {

    printf ("------------HASH TABLE Print--------------\n");

    printf ("No.\ttyp\tname\tvalue\tdeclar\tused\n");

    for ( int i=0; i<TABLEMAX; i++ ) {
        printf ("%d:",i);
        tTableItem ptr = table->items[i];
        while ( ptr != NULL ) {

            printf (" (%d,",ptr->type);
            printf("%s",ptr->name.str);
            if (ptr->type==1)printf (",%g",ptr->value.Vnum);
            if (ptr->type==2)printf (",%s  -----length:%d,  alloc size:%d-----",ptr->value.Vstr.str,ptr->value.Vstr.length,ptr->value.Vstr.allocSize);
            if (ptr->type==3)printf (",%d",ptr->value.Vbool);
            printf (",%d,%d)",ptr->declar,ptr->used);

            ptr = ptr->next;
        }
        printf ("\n");
    }
    printf ("------------HASH TABLE Print END--------------\n");
}
