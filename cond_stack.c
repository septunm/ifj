/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

#include "cond_stack.h"


void initStackCond(TStackCond *s){
     s->first = NULL;
     s->count = 0;
}

void pushStackCond(TStackCond *s, int cond_done, struct listItem *saveAddrActive){
          
    TStackCondItem *temp;
    // alokace noveho prvku
    temp = malloc(sizeof(TStackCondItem));
    if(temp == NULL) return;
    // prirazeni hodnoty a provazani s vedlejsim 
    temp->cond_done = cond_done;
    temp->saveAddrActive = saveAddrActive;
    
    temp->next = s->first;
    s->first = temp;
    (s->count)++;
}

void freeStackCond(TStackCond *s){    
     // pomocny ukazatel
    TStackCondItem *help;

    //cyklus pro naplneny seznam a postupne odmazavani polozek
    while(s->first != NULL){
        help = s->first;
        s->first = s->first->next;
        free(help);
    }
    s->count = 0;
    s->first = NULL;
}

void popStackCond(TStackCond *s){
     TStackCondItem *temp;
     if(s != NULL){
       if(s->count > 0){       
          temp = s->first;
          s->first = s->first->next;
          --(s->count);
          free(temp);       
       }           
     }
}

int topStackCond(TStackCond *s){
    return s->first->cond_done;      
}

struct listItem *topStackAddr(TStackCond *s){
    return s->first->saveAddrActive;      
}

